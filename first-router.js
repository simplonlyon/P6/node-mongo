const router = require('express').Router();
//Créer une variable dogs avec un tableau vide dedans
const dogs = [];

//Faire une route / en get qui renvoie le tableau en json
router.get('/', (request, response) => response.json(dogs));
//Faire une route / en post qui récupère le body et le push dans
//le tableau
router.post('/', (request, response) => {
    dogs.push(request.body);
    response.status(201).json(request.body);
});

//côté index.js, faire un require de ./first-router et le use sur 
//l'url /dog

module.exports = router;