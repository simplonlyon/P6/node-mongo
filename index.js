const express = require('express');
const bodyParser = require('body-parser');

// const repo = require('./first-repository');
// repo.findAll().then(dog => console.log(dog));


//On crée une application express (qui est un serveur en fait)
const app = express();
//On enregistre le middleware bodyParser.json et urlencoded pour
//que notre application express décode automatiquement son body
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//On crée un router pour enregistrer les différentes routes dessus
//const router = express.Router();
//On dit à express d'utiliser le router quand on va sur l'url /api
//app.use('/api', router);
//On enregistre une route sur le router
// router.get('/', function(request, response) {
//     response.json({test:'bloup'});
//});

app.use('/dog', require('./first-router'));
app.use('/restaurant', require('./restaurant-router'));

//On dit à l'application d'écouter le port 3000
app.listen(3000);
