const router = require('express').Router();
const repo = require('./restaurant-respository');



router.get('/', (request, response) => {
    /**
     * Petite pagination pour faire qu'on aille pas chercher les 40000 trucs 
     * à chaque requête.
     * On va chercher dans les paramètres de la query les valeurs de
     * page et pageSize, et avec un opérateur terneur (un genre de if)
     * on demande si le truc est présent, si oui on le parseInt et on l'assigne à la variable
     * si il n'est pas présent, on met la valeur par défaut
     */
    let page = request.query.page ? parseInt(request.query.page) : 1;
    let pageSize = request.query.pageSize ? parseInt(request.query.pageSize): 25  ;
    /**
     * On donne à notre findAll le nombre de trucs à afficher et
     * la "page" à laquelle on se trouve
     */
    repo.findAll(pageSize, (page-1)*pageSize)
    .then(data => response.json(data))
    .catch((err) => response.status(500).json(err.message));;
});

router.post('/', (request, response) => {
    //On déclenche la méthode du repository
    repo.add(request.body)
    //dans le then on fait le response.json
    .then(data => response.json(data))
    //Si erreur, on renvoie un status 500 avec le message d'erreur
    .catch((err) => response.status(500).json(err.message));;
});

router.get('/:id', (request, response) => {
    //On chope le :id de l'url avec request.params.id
    repo.find(request.params.id)
    .then(data => response.json(data))
    .catch((err) => response.status(500).json(err.message));;
});

router.delete('/:id', (request, response) => {
    repo.remove(request.params.id)
    .then(data => response.status(200).end())
    .catch((err) => response.status(500).json(err.message));
});

module.exports = router;