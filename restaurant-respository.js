const {getCollection} = require('./first-repository');

const {ObjectId} = require('mongodb');


async function findAll(limit = 25, skip = 0) {
    const col = await getCollection('restaurants');
    //On peut faire sur la collection quasiment exactement les
    //mêmes méthodes/requêts qu'avec le client mongodb en cli
    return col.find().skip(skip).limit(limit).toArray();
}

async function find(id) {
    const col = await getCollection('restaurants');
    //On peut faire sur la collection quasiment exactement les
    //mêmes méthodes/requêts qu'avec le client mongodb en cli
    return col.findOne({_id: new ObjectId(id)});
}

async function add(restaurant) {
    const col = await getCollection('restaurants');

    await col.insertOne(restaurant);

    return restaurant;
}

async function remove(id) {
    const col = await getCollection('restaurants');
    return col.deleteOne({_id:new ObjectId(id)});

}

module.exports = {
    findAll:findAll,
    find:find,
    add:add,
    remove:remove
}